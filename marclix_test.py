import requests 
import pandas as pd

def main(counts,input_file):
  output_file = input_file + "_out"
  output_stat_file = input_file + "_stat"
  input = pd.read_csv(input_file, sep="\t", header=None)
  input.columns = ["cp_id","lander_name", "expected_landing"]
  landing_campid_dict = {}
  for index, row in input.iterrows():
    landing_campid_dict[row['expected_landing']] = row['cp_id']
 
  output_columns = ["result_landing","result_cp_id", "cp_id", "expected_landing"]
  data = pd.DataFrame(columns=output_columns)
  print("Tests are rolling ... ! ")
  for count in range(counts):
    for index, row in input.iterrows():
      result = requests.get(create_URL(row['cp_id']),allow_redirects=True).url
      data.loc[len(data)] = [result,landing_campid_dict.get(result, ""), row['cp_id'],row['expected_landing']]
    print("Test loop %s is done" %count)
  data.to_csv(output_file,sep="\t")
  # Single thread with exhausted i/o. wtf is that shit?
  # Loop and open these two fucking files again and again ???.
  #with open(input_file) as f:
  #  with open(output_file, "w") as f1:
  #    for line in f:
  #      result = requests.get(create_URL(line),allow_redirects=True).url
  #      f1.write(result +"\t" + line )  
  ## Too much IO. -> cache in mem ??
  ## Too big file -> external mem.!!     
  #data = pd.read_csv(output_file, sep="\t", header=None)
  #data.columns = ["result_landing", "cp_id","lander_name", "expected_landing"]

  true_pos = data[["result_cp_id", "cp_id"]].assign(NE=data.result_cp_id == data.cp_id)
  with open(output_stat_file, 'a') as f:
    f.write("Total test loop : %s \n" %counts)
    (true_pos['NE'].value_counts(normalize=True) * 100).to_csv(f, sep='\t')
    (data[['result_cp_id','result_landing']].groupby(['result_cp_id'])['result_landing'].value_counts(normalize=True) * 100).to_csv(f,sep="\t")

def create_URL_from_line(line):
  arr = line.split("\t")
  URL = ("http://traxclix.mobiclix.io/click?c=%s" % arr[0])
  return URL

def create_URL(cp_id):
  URL = ("http://traxclix.mobiclix.io/click?c=%s" % cp_id)
  return URL  

if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser(description='Create a parser schema')
  parser.add_argument('--c', metavar='num', required=True,
                      help='number of loops')
  parser.add_argument('--i', metavar='path', required=True,
                      help='path to input file')
  args = parser.parse_args()
  main(counts=int(args.c), input_file=args.i)