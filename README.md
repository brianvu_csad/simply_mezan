Bitbucket markdown editer is fucking stupid!

 Simple logic test for landing page routing.
* Logic test :  
    *  if the landing page be routed correctly or not?  
    *  percentage of each landing page.   
### Prequisite: 
- pandas  
- input file in format [campain_id] [landing_page_name] [landing_page_url]  
### Usage :  
> optional arguments:  
>  -h, --help  show this help message and exit  
>     --c num     number of loops  
>     --i path    path to input file  
> 
> python marclix_test.py --c 100 --i input_file    

There will be an output file : *_stat that has the following format :  
> False   75.0  
> True    25.0  
> 1       https://www.mocoplay.com/       60.0  
> 1       https://www.dantri.com/         40.0  
> 2       https://www.mocoplay.com/       100.0  
> 3       https://www.mocoplay.com/       100.0  
> 2239    https://www.mocoplay.com/       100.0  
    
The first two lines describe the false positive percentage and true positive percentage of  landing pages routing.   
The rest are percentages of landing page by campain id. 